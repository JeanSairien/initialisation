class Case {
	constructor(y,x){	//Paramètres d'une case
		this.y = y;
		this.x = x;
		this.bombe = false;
		this.valeur = 0;
		this.ouvert = constant.open_cell.NO;
	}
	clicked(){			//Fonction utilisé lorsqu'une case est cliquée
		if(mouseButton == LEFT){	//Clic droit
			if(this.bombe && this.ouvert!=constant.open_cell.FLAG)
				etat = constant.state_game.LOOSED;
			if(this.ouvert == constant.open_cell.NO)
				this.ouvert = constant.open_cell.YES;
			if(this.valeur == 0){
				tab0.push({y:Math.floor(this.y / tailleCase)+1, x :Math.floor(this.x/tailleCase)});
				devoilerZeros();		//Fonction permettant de dévoiler tous les 0 autour de la case cliquée
			}
		}
		if(mouseButton == RIGHT){	//clic gauche
			if(this.ouvert == constant.open_cell.NO)
				return (this.ouvert = constant.open_cell.FLAG);
			if(this.ouvert == constant.open_cell.FLAG)
				return (this.ouvert = constant.open_cell.NO);
		}
	}
	drawing(){		//Fonction permettant d'écrire la valeur des cases									
		fill(50);
		rect(this.x, this.y, tailleCase, tailleCase);
		if(this.ouvert == constant.open_cell.YES){
			fill(150);
			rect(this.x, this.y, tailleCase, tailleCase);
			if(this.valeur == 1)
				fill(0, 0, 255);	//Bleu
			if(this.valeur == 2)
				fill(0, 200, 0);	//Vert
			if(this.valeur == 3)
				fill(255, 0, 0);	//Rouge
			if(this.valeur == 4)
				fill(0, 0, 100);	//Bleu foncé
			if(this.valeur == 5)
				fill(150, 0, 0);	//Rouge foncé
			if(this.valeur == 6)
				fill(0);			//Noir
			if(this.valeur == 7)
				fill(0);
			if(this.valeur == 8)
				fill(0);
			if(this.valeur == "💣")
				fill(0);
			textSize(tailleNombre);
			if(this.valeur != 0)
				text(this.valeur, this.x + tailleNombre/2, this.y+ tailleCase- tailleNombre/2);
		}
		if(this.ouvert == constant.open_cell.FLAG){		//Cas où la case est un drapeau
			fill(255);
			textSize(tailleNombre);
			text("⛳", this.x + tailleNombre/2, this.y+ tailleCase- tailleNombre/2);
		}
	}
	over(){				//Dessine un carré transparent de couleur lorsque la souris est dessus
		overCase = true;
		fill(255, 0, 0, 80);
		rect(this.x, this.y, tailleCase, tailleCase);				
	}
}