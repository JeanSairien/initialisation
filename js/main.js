function setup(){		//Initialise le canvas
	createCanvas(2000, 2000);
	initialisationTab();
	frameRate(60);
	document.querySelector('body').addEventListener('contextmenu', e => e.preventDefault()); //Supprime le menu du clic droit sur Chrome
}

function draw(){	//Boucle principale du jeu
	background(255);	//Fonc blanc
	let compteurVictoire = 0;
	compteurMineTrouvee = 0;
	for (let i = 1; i < tabCase.length ; i++) 
		for (let j = 0; j < tabCase[1].length; j++){
			tabCase[i][j].drawing();
			if(tabCase[i][j].ouvert == constant.open_cell.FLAG)		//Vérifie les bombes trouvées
				compteurMineTrouvee++;
			if(tabCase[i][j].ouvert == constant.open_cell.YES)		//Vérifie les cases ouvertes
				compteurVictoire++
			if(compteurVictoire == nombreCaseLongueur * nombreCaseLargeur - nombreDeMines && etat == constant.state_game.PLAYING)
				etat = constant.state_game.WON;						//Défini l'état de victoire
		} 	
	mouseOver();	//Lance les fonctions secondaires
	info();
	finDuJeu();
}

function mousePressed(){	//Réceptionne les clics souris
	if(!timerStart && 10 < mouseX + 1 && 10 + tailleCase*nombreCaseLargeur-nombreCaseLargeur > mouseX && 10 < mouseY + 1 && tailleCase*nombreCaseLongueur-nombreCaseLongueur + 10 > mouseY && etat == constant.state_game.START){	//Concerne le tout premier clic souris
		choixMines(Math.floor(mouseY/tailleCase+1),Math.floor(mouseX/tailleCase));	//Choix des mines en fonctions du premier clic
		valeurCase();	//Calcule la valeur de toutes les cases
		startTimer = +new Date();	//Démarre le timer
		timerStart = true;	//Empêche un autre passage cette condition
		etat = constant.state_game.PLAYING;	//Définit l'état du jeu en "jouable"
	}
	if(etat == constant.state_game.PLAYING && 10 < mouseX + 1 && 10 + tailleCase*nombreCaseLargeur-nombreCaseLargeur > mouseX && 10 < mouseY + 1 && tailleCase*nombreCaseLongueur-nombreCaseLongueur + 10 > mouseY)
		tabCase[Math.floor(mouseY/tailleCase+1)][Math.floor(mouseX/tailleCase)].clicked();	//Lance la fonction lorsqu'une case est cliquée
}

function mouseOver(){	//Réceptionne la souris
	if(etat == constant.state_game.PLAYING && 10 < mouseX + 1 && 10 + tailleCase*nombreCaseLargeur-nombreCaseLargeur > mouseX && 10 < mouseY + 1 && tailleCase*nombreCaseLongueur-nombreCaseLongueur + 10 > mouseY)	
		tabCase[Math.floor(mouseY/tailleCase+1)][Math.floor(mouseX/tailleCase)].over();
}

		
