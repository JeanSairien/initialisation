		const tabCase = [[]];	//Le tableau comprend toutes les cases de la partie sous forme d'objet
		const nombreCaseLargeur = 10;	
		const nombreCaseLongueur = 10;
		const nombreDeMines = 10;
		const tailleCase = 80;	
		const tailleNombre = tailleCase * 3/5; //Définit la taille du texte en fonction de la taille d'une case
		const tab0 = [0];	//Regroupe l'ensemble des 0 qui doivent être dévoilés, ce tableau est souvent vide
		const constant = {	//Toutes les constantes utilisées
			state_game: {
				START: 0,
				PLAYING: 1,
				WON: 2,
				LOOSED: 3
			},
			open_cell:{
				YES: true,
				FLAG: null,
				NO: false
			}
		}

		let etat = constant.state_game.START;
		let timerStart = false;
		let startTimer= +new Date(), timer;
		let compteurMineTrouvee = 0;	//Compte le nombre total de mine trouvées
		let overCase = false;	//Permet de savoir si la souris est au dessus d'une case

/*Ici sont stockées toutes les variables du projet*/