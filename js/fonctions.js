function info(){	//Donne les informations sur la partie
	fill(0);
	textSize(tailleNombre);
	if(timerStart)
		timer = +new Date();
	if(timer)
		text(Math.floor((timer-startTimer)/ 1000), tailleCase/2, tailleCase*nombreCaseLongueur+tailleNombre);	//Timer
	text(nombreDeMines - compteurMineTrouvee, tailleCase*nombreCaseLargeur - tailleCase, tailleCase*nombreCaseLongueur+tailleNombre);	//Nombre de mines découvertes
}

function devoilerZeros(){	//Permet de dévoiler les cases autour d'un zero ainsi que d'enchainer la découverte de tous les autres 0
	while(tab0.length >1){	//La fonction est éxécuté tant que le tableau n'est pas vide
		if(tab0[1].y-1 >=1){	//Vérifie les 3 cases du dessus
			if(tab0[1].x-1>=0){
				if(tabCase[tab0[1].y-1][tab0[1].x-1].valeur == 0 && tabCase[tab0[1].y-1][tab0[1].x-1].ouvert == constant.open_cell.NO)
					tab0.push({x:tabCase[tab0[1].y-1][tab0[1].x-1].x/tailleCase, y:tabCase[tab0[1].y-1][tab0[1].x-1].y/tailleCase+1});
				if(tabCase[tab0[1].y-1][tab0[1].x-1].ouvert == constant.open_cell.NO)
					tabCase[tab0[1].y-1][tab0[1].x-1].ouvert = constant.open_cell.YES;
			}
			if(tabCase[tab0[1].y-1][tab0[1].x].valeur == 0 && tabCase[tab0[1].y-1][tab0[1].x].ouvert == constant.open_cell.NO)
				tab0.push({x:tabCase[tab0[1].y-1][tab0[1].x].x/tailleCase, y:tabCase[tab0[1].y-1][tab0[1].x].y/tailleCase+1});
			if(tabCase[tab0[1].y-1][tab0[1].x].ouvert == constant.open_cell.NO)
				tabCase[tab0[1].y-1][tab0[1].x].ouvert = constant.open_cell.YES;
			if(tab0[1].x+1<=nombreCaseLargeur-1){
				if(tabCase[tab0[1].y-1][tab0[1].x+1].valeur == 0 && tabCase[tab0[1].y-1][tab0[1].x+1].ouvert == constant.open_cell.NO)
					tab0.push({x:tabCase[tab0[1].y-1][tab0[1].x+1].x/tailleCase, y:tabCase[tab0[1].y-1][tab0[1].x+1].y/tailleCase+1});
				if(tabCase[tab0[1].y-1][tab0[1].x+1].ouvert == constant.open_cell.NO)
					tabCase[tab0[1].y-1][tab0[1].x+1].ouvert = constant.open_cell.YES;
			}
		}
		if(tab0[1].x-1>=0){	//Vérifie les 2 cases autour de la case principale
			if(tabCase[tab0[1].y][tab0[1].x-1].valeur == 0 && tabCase[tab0[1].y][tab0[1].x-1].ouvert == constant.open_cell.NO)
				tab0.push({x:tabCase[tab0[1].y][tab0[1].x-1].x/tailleCase, y:tabCase[tab0[1].y][tab0[1].x-1].y/tailleCase+1});
			if(tabCase[tab0[1].y][tab0[1].x-1].ouvert == constant.open_cell.NO)
				tabCase[tab0[1].y][tab0[1].x-1].ouvert = constant.open_cell.YES;
		}
		if(tab0[1].x+1<=nombreCaseLargeur-1){
			if(tabCase[tab0[1].y][tab0[1].x+1].valeur == 0 && tabCase[tab0[1].y][tab0[1].x+1].ouvert == constant.open_cell.NO)
				tab0.push({x:tabCase[tab0[1].y][tab0[1].x+1].x/tailleCase, y:tabCase[tab0[1].y][tab0[1].x+1].y/tailleCase+1});
			if(tabCase[tab0[1].y][tab0[1].x+1].ouvert == constant.open_cell.NO)
				tabCase[tab0[1].y][tab0[1].x+1].ouvert = constant.open_cell.YES;
		}
		if(tab0[1].y+1<=nombreCaseLongueur){	//Vérifie les 3 cases du dessous
			if(tab0[1].x-1>=0){
				if(tabCase[tab0[1].y+1][tab0[1].x-1].valeur == 0 && tabCase[tab0[1].y+1][tab0[1].x-1].ouvert == constant.open_cell.NO)
					tab0.push({x:tabCase[tab0[1].y+1][tab0[1].x-1].x/tailleCase, y:tabCase[tab0[1].y+1][tab0[1].x-1].y/tailleCase+1});
				if(tabCase[tab0[1].y+1][tab0[1].x-1].ouvert == constant.open_cell.NO)
					tabCase[tab0[1].y+1][tab0[1].x-1].ouvert = constant.open_cell.YES;
			}
			if(tabCase[tab0[1].y+1][tab0[1].x].valeur == 0 && tabCase[tab0[1].y+1][tab0[1].x].ouvert == constant.open_cell.NO)
				tab0.push({x:tabCase[tab0[1].y+1][tab0[1].x].x/tailleCase, y:tabCase[tab0[1].y+1][tab0[1].x].y/tailleCase+1});
			if(tabCase[tab0[1].y+1][tab0[1].x].ouvert == constant.open_cell.NO)
				tabCase[tab0[1].y+1][tab0[1].x].ouvert = constant.open_cell.YES;
			if(tab0[1].x+1<=nombreCaseLargeur-1){
				if(tabCase[tab0[1].y+1][tab0[1].x+1].valeur == 0 && tabCase[tab0[1].y+1][tab0[1].x+1].ouvert == constant.open_cell.NO)
					tab0.push({x:tabCase[tab0[1].y+1][tab0[1].x+1].x/tailleCase, y:tabCase[tab0[1].y+1][tab0[1].x+1].y/tailleCase+1});
				if(tabCase[tab0[1].y+1][tab0[1].x+1].ouvert == constant.open_cell.NO)
					tabCase[tab0[1].y+1][tab0[1].x+1].ouvert = constant.open_cell.YES;
			}
		}				
		tab0.splice(1,1);	//Supprime le zéro du tableau qui vient d'être observé 		
	}
}	

function finDuJeu(){	//Vérifie dans quel état de jeu on est
	if (etat == constant.state_game.LOOSED){	//Perdu
		fill(255);
		textSize(50);
		text("PERDU", 100, 100);
		for (let i = 1; i < tabCase.length ; i++) 
			for (let j = 0; j < tabCase[1].length; j++) 
				tabCase[i][j].ouvert = constant.open_cell.YES;
		timerStart = false;
	}
	if (etat == constant.state_game.WON){	//Gagné
		fill(255);
		textSize(50);
		text("GAGNE", 100, 100);
		timerStart = false;
	}
}	